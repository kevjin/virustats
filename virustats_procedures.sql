USE virustats;

DROP PROCEDURE IF EXISTS track_restrictions;
DELIMITER $$
CREATE PROCEDURE track_restrictions(IN c_name VARCHAR(45))
BEGIN
	SELECT country_name, restriction_level FROM country countries JOIN travel_restriction restrictions ON countries.country_name = restrictions.to_country
    WHERE restrictions.from_country = c_name;
END	$$

DROP PROCEDURE IF EXISTS track_nationwide_levels;
DELIMITER $$
CREATE PROCEDURE track_nationwide_levels(IN n_level VARCHAR(45))
BEGIN
	SELECT country_name FROM country WHERE nationwide_level = n_level;
END	$$

DROP PROCEDURE IF EXISTS new_cases;
DELIMITER $$
CREATE PROCEDURE new_cases(IN c_name VARCHAR(45))
BEGIN
     SELECT now.total_cases - past.total_cases FROM
    ((SELECT total_cases FROM datalog ORDER BY record_timestamp DESC LIMIT 0,1) AS now, (SELECT total_cases FROM datalog ORDER BY record_timestamp DESC LIMIT 1,1) AS past);
END	$$

CALL new_cases('China');

DROP PROCEDURE IF EXISTS new_deaths;
DELIMITER $$
CREATE PROCEDURE new_deaths(IN c_name VARCHAR(45))
BEGIN
     SELECT now.total_deaths - past.total_deaths FROM
    ((SELECT total_deaths FROM datalog ORDER BY record_timestamp DESC LIMIT 0,1) AS now, (SELECT total_deaths FROM datalog ORDER BY record_timestamp DESC LIMIT 1,1) AS past);
END	$$

CALL new_deaths('China');

DROP PROCEDURE IF EXISTS active_cases;
DELIMITER $$
CREATE PROCEDURE active_cases(IN c_name VARCHAR(45))
BEGIN
     SELECT now.total_cases - now.total_deaths - now.total_recovered FROM
    (SELECT * FROM datalog ORDER BY record_timestamp DESC LIMIT 0,1) AS now;
END	$$

CALL active_cases('China');

DROP PROCEDURE IF EXISTS create_datalog;
DELIMITER $$
CREATE PROCEDURE create_datalog(IN d_id INT, IN c_name VARCHAR(45), IN r_timestamp timestamp, IN t_cases INT, IN t_deaths INT, IN t_recovered INT, IN t_tested INT)
BEGIN
	INSERT INTO datalog(data_ID, country_name, record_timestamp, total_cases, total_deaths, total_recovered, total_tested) VALUES (d_id, c_name, r_timestamp, t_cases, t_deaths, t_recovered, t_tested); 
END	$$

CALL create_datalog(123, 'China', '2008-01-01 00:00:01', 100, 100, 100, 100);
CALL create_datalog(1234, 'China', '2008-01-05 00:00:01', 1000, 500, 200, 200);
CALL create_datalog(12356, 'China', '2008-01-06 00:00:01', 1500, 1000, 300, 300);
CALL create_datalog(1235, 'China', '2008-01-07 00:00:01', 2000, 1000, 300, 300);

DROP PROCEDURE IF EXISTS create_restriction;
DELIMITER $$
CREATE PROCEDURE create_restriction(IN from_name VARCHAR(45), IN to_name VARCHAR(45), IN r_level INT)
BEGIN
	INSERT INTO travel_restriction(from_country, to_country, restriction_level) VALUES (from_name, to_name, r_level); 
END	$$

CALL create_restriction('China', 'Mexico', 3);
CALL create_restriction('China', 'United States', 3);

DROP PROCEDURE IF EXISTS create_country;
DELIMITER $$
CREATE PROCEDURE create_country(IN c_name VARCHAR(45), IN pop INT, IN n_level INT, IN s_info VARCHAR(255), IN p_number VARCHAR(45))
BEGIN
	INSERT INTO country(country_name, population, nationwide_level, site_info, phone_number) VALUES (c_name, pop, n_level, s_info, p_number); 
END	$$

CALL create_country('United States', 400000000);
CALL create_country('Mexico', 100);

DROP PROCEDURE IF EXISTS read_country;
DELIMITER $$
CREATE PROCEDURE read_country(IN c_name VARCHAR(45))
BEGIN
	SELECT country_name, population FROM country WHERE country_name = c_name;
END $$

CALL read_country('United States');

DROP PROCEDURE IF EXISTS update_country;
DELIMITER $$
CREATE PROCEDURE update_country(IN c_name VARCHAR(45), IN pop INT, IN n_level INT, IN s_info VARCHAR(255), IN p_number VARCHAR(45))
BEGIN
	UPDATE country
    SET population = pop,
	nationwide_level = n_level,
    site_info = s_info,
    phone_number = p_number
    WHERE country_name = c_name;
END $$

DROP PROCEDURE IF EXISTS delete_country;
DELIMITER $$
CREATE PROCEDURE delete_country(IN c_name VARCHAR(45))
BEGIN
	DELETE FROM country WHERE country_name = c_name;
END $$

CALL delete_country('United States');