/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
db.mysql.url="jdbc:mysql://localhost:3306/db?characterEncoding=UTF-8&useSSL=false"
*/
//package javamysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.mysql.cj.jdbc.CallableStatement;
//import com.sun.tools.javac.code.Types;

import java.util.*;

/**
 *
 * @author kath
 */
public class Virustats {

  /** The name of the MySQL account to use (or empty for anonymous) */
  private static String userName;

  /** The password for the MySQL account (or empty for anonymous) */
  private static String password;

  /** The name of the computer running MySQL */
  private final String serverName = "localhost";

  /** The port of the MySQL server (default is 3306) */
  private final int portNumber = 3306;

  /**
   * The name of the database we are testing with (this default is installed with
   * MySQL)
   */
  private final String dbName = "Virustats";

  /** The name of the table we are testing with */
  private final String tableName = "test";
  private final boolean useSSL = false;

  private ArrayList<String> countries = new ArrayList<>();
  private ArrayList<String> regions = new ArrayList<>();


  /**
   * Get a new database connection
   * 
   * @return
   * @throws SQLException
   */
  public Connection getConnection() throws SQLException {
    Connection conn = null;
    Properties connectionProps = new Properties();
    connectionProps.put("user", userName);
    connectionProps.put("password", password);

    conn = DriverManager.getConnection("jdbc:mysql://" + this.serverName + ":" + this.portNumber
        + "/" + this.dbName + "?serverTimezone=UTC", connectionProps);

    return conn;
  }

  /**
   * Run a SQL command which does not return a recordset:
   * CREATE/INSERT/UPDATE/DELETE/DROP/etc.
   * 
   * @throws SQLException If something goes wrong
   */
  public boolean executeUpdate(Connection conn, String command) throws SQLException {
    Statement stmt = null;
    try {
      stmt = conn.createStatement();
      stmt.executeUpdate(command); // This will throw a SQLException if it fails
      return true;
    }
    finally {

      // This will run whether we throw an exception or not
      if (stmt != null) {
        stmt.close();
      }
    }
  }

  /**
   * Connect to MySQL and do some stuff.
   */
  public void run(Virustats app, Scanner scanner) {

    // Connect to MySQL
    Connection conn = null;
    try {
      conn = this.getConnection();
    }
    catch (SQLException e) {
      System.out.println("ERROR: Could not connect to the database");
      e.printStackTrace();
      return;
    }
    String queryCountries = "SELECT country_name FROM country";
    String queryRegions = "SELECT region_name FROM region";

    retrieveData(conn, queryCountries, "country_name", countries);
    retrieveData(conn, queryRegions, "region_name", regions);
    app.inputAction(app, scanner);
  }
  
  void retrieveData(Connection conn, String query, String resultField, ArrayList<String> list) {
    try (PreparedStatement ps = conn.prepareStatement(query); 
        ResultSet results = ps.executeQuery()) {
      while (results.next()) {
        String item = results.getString(resultField);
        list.add(item.toLowerCase());
      }
    }
    catch (SQLException e) {
      System.out.println("ERROR: Could not retrieve data");
      e.printStackTrace();
      return;
    }
  }

  String params(int paramsLength) {
    String result = "?";
    while (paramsLength > 1) {
      result += ", ?";
      paramsLength -= 1;
    }
    return result;
  }

  public void callProcedure(String procedureName, String... params) {
    Connection connection = null;
    CallableStatement statement = null;
    ResultSet rs = null;
    try {
      connection = this.getConnection();
      statement = (CallableStatement) connection
          .prepareCall("{call " + procedureName + "(" + params(params.length) + ")}");
      for (int i = 0; i < params.length; i += 1) {
        statement.setString(i + 1, params[i]);
      }
      boolean hadResults = statement.execute();
      System.out.println("Success!");
      while (hadResults) {
        rs = statement.getResultSet();
        while (rs.next()) {
          String country = rs.getString("country_name");
          String population = rs.getString("population");
          System.out.println("Country: " + country);
          System.out.println("Population: " + population);
        }
        hadResults = statement.getMoreResults();
      }
    }
    catch (SQLException e) {
      System.out.println("ERROR: Could not call procedure");
      e.printStackTrace();
    }
    finally {
      try {
        connection.close();
      }
      catch (SQLException e) {
        System.out.println("ERROR: Could not close connection");
      }
    }
  }

  public void inputAction(Virustats app, Scanner scanner) {
    System.out.println("Choose action: ");
    System.out.println("create");
    System.out.println("read");
    System.out.println("update");
    System.out.println("delete");
    System.out.println("");
    String action = scanner.nextLine();

    if (!action.equals("create") && !action.equals("read") && !action.equals("update")
        && !action.equals("delete")) {
      System.out.println("ERROR: Choose an appropriate action");
      app.inputAction(app, scanner);
    }
    System.out.println("Choose entity to " + action + ": ");
    System.out.println("country");
    System.out.println("region");
    System.out.println("hospital");
    System.out.println("travel restriction");
    String entity = scanner.nextLine();
    if (entity.equals("country")) {
      app.operateCountry(app, scanner, action);
    } else if (entity.equals("region")) {
      app.operateRegion(app, scanner, action);
    }
//    else if (entity.equals("hospital")) {
//      app.operateHospital(app, scanner, action);
//    } else if (entity.equals("travel restriction")) {
//      app.operateTravelRestriction(app, scanner, action);
//    }
  }

  public void operateCountry(Virustats app, Scanner scanner, String action) {
    System.out.println("Enter country name");
    String country = scanner.nextLine().toLowerCase();

    if (action.equals("create")) {
      if (countries.contains(country)) {
        System.out.println("ERROR: country already exists");
        app.inputAction(app, scanner);
      }
      else {
        System.out.println("Enter country population");
        String population = scanner.nextLine();
        app.callProcedure("create_country", country, population);
      }
    }
    else {
      if (!countries.contains(country)) {
        System.out.println("ERROR: country does not exist");
        inputAction(app, scanner);
      }
      else if (action.equals("read")) {
        app.callProcedure("read_country", country);
      }
      else if (action.equals("update")) {
        System.out.println("Enter new country population");
        String population = scanner.nextLine();
        app.callProcedure("update_country", country, population);
      }
      else if (action.equals("delete")) {
        app.callProcedure("delete_country", country);
        countries.remove(country);
      }
    }
    app.run(app, scanner);
  }
  
  public void operateRegion(Virustats app, Scanner scanner, String action) {
    System.out.println("Enter region name");
    String region = scanner.nextLine().toLowerCase();

    if (action.equals("create")) {
      if (countries.contains(region)) {
        System.out.println("ERROR: region already exists");
        app.inputAction(app, scanner);
      }
      else {
        System.out.println("Enter country name of region");
        String country = scanner.nextLine().toLowerCase();
        System.out.println("Enter region population");
        String population = scanner.nextLine();
        app.callProcedure("create_region", region, country, population);
      }
    }
    else {
      if (!countries.contains(region)) {
        System.out.println("ERROR: region does not exist");
        inputAction(app, scanner);
      }
      else if (action.equals("read")) {
        app.callProcedure("read_country", region);
      }
      else if (action.equals("update")) {
        System.out.println("Enter new country population");
        String population = scanner.nextLine();
        app.callProcedure("update_country", region, population);
      }
      else if (action.equals("delete")) {
        app.callProcedure("delete_country", region);
        countries.remove(region);
      }
    }
    app.run(app, scanner);
  }
  
  

  /**
   * Connect to the DB and do some stuff
   * 
   * @param args
   */
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter username");
    userName = scanner.nextLine();
    System.out.println("Enter password");
    password = scanner.nextLine();
    Virustats app = new Virustats();
    app.run(app, scanner);
    System.exit(0);
  }
}
